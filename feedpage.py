#!/usr/bin/env python3

# feedpage.py

"""
Description: Contains AggegatorPage and Feed classes used by
    aggregator.py, and template html for generating the page
Author: Yonder
Date Created: November 1, 2023
Date Modified: November 10, 2023
Version: 1.0
Python Version: 3.11.6
Dependencies: feedparser, requests
License: AGPLv3
"""

import requests
import random
import feedparser
import urllib.robotparser
from urllib.parse import urlparse
from datetime import datetime

class AggregatorPage:
    def __init__(self, cfg):
        self.cfg = cfg
        self.releases_block_html = ""
        self.artists_block = set()
        self.template_index_html = index_template
        self.release_template_html = release_template
        self.index_html = ""
        self.artist_covers = {}
        self.artists_to_highlight = []
        
    def setArtistsToHighlight(self, artists_to_highlight):
        self.artists_to_highlight = artists_to_highlight

    def addToReleasesBlock(self, releases_html):
        self.releases_block_html += releases_html
        
    def generatePage(self):
        # first, prepare the html for the artists block
        artists_block_html = ''.join(self.artists_block)
        faircamp_site = self.cfg.getboolean('SITE', 'Faircamp Site', fallback = False)
        include_nav_bar = self.cfg.getboolean('SITE', 'Nav Bar', fallback = True)
        save_as = self.cfg.get('FILES', 'Save As', fallback = 'aggregator.html')
        
        index_html = self.template_index_html
        page_heading = self.cfg.get('TEXT', 'Page Heading', fallback = 'Faircamp Aggregator')
        page_text = self.cfg.get('TEXT', 'Page Text', fallback = 'An aggregator for Faircamp releases.')
        site_name = self.cfg.get('SITE', 'Site Name', fallback = 'Site Name Goes Here')
        agg_link = './' + save_as
        nav_block = '<nav><a class="logo" href="./"><img alt="Faircamp" src="logo.svg"></a><div class="path"><a href="./">' + page_heading + '</a></div></nav>'
        if not include_nav_bar:
            nav_block = ""
        elif faircamp_site:
            nav_block = '<nav><a class="logo" href="./"><img alt="Faircamp" src="logo.svg"></a><div class="path"><a href="./">' + site_name + '</a><span>›</span><a href="' + agg_link + '">' + page_heading + '</a></div></nav>'
        
        self.index_html = (
            index_html.replace('NAV_BLOCK', nav_block )
            .replace('PAGE_HEADING', page_heading )
            .replace('SITE_NAME', site_name )
            .replace('AGG_LINK', agg_link )
            .replace('PAGE_TEXT', page_text )
            .replace('ARTISTS_BLOCK', artists_block_html )
            .replace('RELEASES_BLOCK', self.releases_block_html )
        )
    
    def generateRelease(self, row):
        build_date = row['date_full']
        website = row['site']
        # Get a nice simplified date from build_date. First, get the values from the full string
        date_obj = datetime.strptime(build_date, '%a, %d %b %Y %X %z')
        # Now use those values to construct a straightforward date string
        date_string = date_obj.strftime("%d %b %Y")
        artist = row['artist']
        title = row['title']
        link_url = row['link']
        
        # remove '0' from the beginning of the date string, if it's there
        if date_string[0] == '0':
            date_string = date_string[1:]
        
        #  Instead of a cover, use a small linear gradient for each artist
        if artist not in self.artist_covers:
            # base color.
            color_base = self.cfg.get('RELEASES', 'Artist Cover Color 1', fallback = '#FFFFFF')
            if color_base.lower() == 'random':
                color_base = "#" + ''.join([random.choice('0123456789ABCDEF') for i in range(6)])
                
            # color to blend
            color_blend = self.cfg.get('RELEASES', 'Artist Cover Color 2', fallback = '#778899')
            if color_blend.lower() == 'random':
                color_blend = "#" + ''.join([random.choice('0123456789ABCDEF') for i in range(6)])
                
            # keep a temporary record of the colours, so all releases by an artist get
            # the same colour. Just to help group their releases visually.
            self.artist_covers[artist] = [color_base, color_blend]
        
        # Deal with titles containing long words (these can affect styling
        # for the whole releases block so we'll put a break point in long words)
        break_long_title_words = self.cfg.getboolean('TEXT', 'Break Long Title Words', fallback = False)
        if break_long_title_words:
            title_words = title.split()
            for w in range(len(title_words)):
                if len(title_words[w]) > 18:
                    break_point = len(title_words[w])//2
                    word_start = title_words[w][:break_point]
                    word_end = title_words[w][break_point:]
                    title_words[w] =  word_start + '<wbr>' + word_end
                    
            title = ' '.join(title_words)
        
        # Create release html from template
        release_html = self.release_template_html
        date_color = self.cfg.get('RELEASES', 'Date Color', fallback = '#778899')
        release_html = (
            release_html.replace('RELEASE_LINK', link_url)
            .replace('SITE_LINK', website)
            .replace('DATE_REF', date_string)
            .replace('DATE_COLOR', date_color)
            .replace('COLOR1', self.artist_covers.get(artist)[0])
            .replace('COLOR2', self.artist_covers.get(artist)[1])
            .replace('ARTIST_NAME', artist)
            .replace('RELEASE_TITLE', title)
        )
        
        self.releases_block_html += release_html
        
        highlight_all_artists = self.cfg.getboolean('RELEASES', 'Highlight All Artists', fallback = False)
        if (highlight_all_artists) or (artist in self.artists_to_highlight):
            unnamed_artist_text = self.cfg.get('TEXT', 'Unnamed Artists Text', fallback = 'Mystery Artist: click to discover!')
            if artist != unnamed_artist_text:
                artists_block_html = '<a href="' + website + '">' + artist + '</a>'
                self.artists_block.add(artists_block_html)
        
    def save(self, index_destination_path):
        with open (index_destination_path, 'w' ) as index_file:
            index_file.write(self.index_html)

        
class Feed:
    def __init__(self, site):
        self.fab_agent = 'Faircamp-Feed-Bot'
        self.headers = {'User-Agent': self.fab_agent}
        self.rss_formats = ['application/rss+xml', 'application/x-rss+xml']
        self.site = site.strip()
        # remove trailing slash from site address if present
        if self.site[-1] == '/':
            self.site = self.site[:-1]
        self.rss = ""
        
    def canFetchFeed(self, site, feed_url):
        rp = urllib.robotparser.RobotFileParser()
        domain = urlparse(site).scheme + '://' + urlparse(site).netloc
        robots_url = domain + "/robots.txt"
        rp.set_url(robots_url)
        rp.read()
        return rp.can_fetch(self.fab_agent, feed_url)
        
    def fetch(self, report_fetched = True):
        can_parse = False
        site = self.site
        if site != "":
            # if site name doesn't include protocol, we'll try it with https://
            if not site.startswith("https://") and not site.startswith("http://"):
                site = "https://" + site                
            feed_url = site + "/feed.rss"
            try:
                # check robots.txt permission to fetch feed.rss. If allowed, request it
                if self.canFetchFeed(site, feed_url):
                    feed = requests.request(method='GET', url=feed_url, headers=self.headers)
                    # if response ok and content type of resource correct, save file
                    if feed.ok and feed.headers.get('Content-Type') in self.rss_formats:
                        if report_fetched:
                            print('Fetched ' + feed_url)
                        self.rss += feed.text
                        can_parse = True
                    else:
                        # No valid feed.rss file
                        print('No valid feed.rss file for ' + feed_url + '. Content type: ' + feed.headers.get('Content-Type'))
                        print(feed_url + ' response ok? ' + str(feed.ok))
                else:
                    # Download disallowed
                    print('Download disallowed for ' + feed_url)
            except Exception as e:
                # Other errors
                print ("Exception when fetching " + feed_url + " : ", e)
        return can_parse
    
    def parse(self, unnamed_artist_text):
        # Parse the feed with feedparser
        releases = []
        fd = feedparser.parse(self.rss)
        build_date = fd.channel.date
        website = fd.channel.link
        for i in fd['items']:
            artist = i.description
            title = i.title
            link_url = i.link
            guid = i.guid
            
            # Construct a simplified date string which can easily be ordered
            date_object = datetime.strptime(build_date, '%a, %d %b %Y %X %z')
            simple_date = date_object.strftime('%Y %m %d %X')
            
            # Artist info is occasionally missing from the description, so...
            if artist == "A release by":
                artist = unnamed_artist_text
            
            # Artist name for release must currently be derived from description
            if artist.startswith("A release by "):
                artist = artist[13:]
            
            release_data = (guid, simple_date, build_date, artist, title, link_url, website)
            releases.append(release_data)
        
        return releases

release_template = """<div class="release">
    <a href="RELEASE_LINK" style="color: #778899; font-size: var(--subtly-smaller); line-height: 0; word-spacing: 3px; line-height: 1.3; text-align: right;display: block;">DATE_REF</a>
    <div class="cover_listing">
        <a class="image" href="RELEASE_LINK">
    <hr style="border: none; height: 1px; background: linear-gradient(0.25turn, COLOR1, 50%, COLOR2); margin-bottom: 0px;">
</a>

    </div>
    <div style="margin-bottom: 12px;">
        <a href="RELEASE_LINK" style="color: var(--color-text); font-size: var(--subtly-larger);text-align: left;display: block;">
            RELEASE_TITLE
        </a>
        <div><a href="SITE_LINK">ARTIST_NAME</a></div>
    </div>
</div>
"""

index_template = """<!DOCTYPE html>
<html >
    <head>
        <title>PAGE_HEADING</title>
        <meta charset="utf-8">
        <meta name="description" content="PAGE_HEADING">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="favicon.svg" rel="icon" type="image/svg+xml">
        <link href="favicon_light.png" rel="icon" type="image/png" media="(prefers-color-scheme: light)">
        <link href="favicon_dark.png" rel="icon" type="image/png"  media="(prefers-color-scheme: dark)">
        <link href="styles.css" rel="stylesheet">
    </head>
    <body>
        NAV_BLOCK
        <div class="index_split ">
    <div class="catalog">
    
    <div class="catalog_info_padded">
        <h1 style="font-size: var(--largest); margin-top: 1rem;">
            PAGE_HEADING
        </h1>
        <p>PAGE_TEXT</p>

        <div class="artists">ARTISTS_BLOCK</div>
    </div>
</div>

    <div class="releases" id="releases">
    RELEASES_BLOCK
    </div>
</div>
        
    </body>
</html>
"""
