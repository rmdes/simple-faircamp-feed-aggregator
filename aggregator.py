#!/usr/bin/env python3

# aggregator.py

"""
Description: This script requests rss feeds from a list of Faircamp
    sites, then reads them and generates a small html file containing
    an aggregated list of releases ordered by date. It also creates and
    maintains an sqlite database of those releases.
Author: Yonder
Date Created: November 1, 2023
Date Modified: November 10, 2023
Version: 1.0
Python Version: 3.11.6
Dependencies: feedparser, requests
License: AGPLv3
"""

import os
import sys
import sqlite3
import configparser
from feedpage import AggregatorPage, Feed


def main():
    script_directory = os.path.dirname(__file__)
    
    # get configuration from config.cfg file
    cfg = configparser.ConfigParser()
    config_path = os.path.normpath(script_directory + '/config.cfg')
    cfg.read(config_path)
    
    # Whether to request new feeds
    refresh = False
    refresh_from_cl = []
    for i in range(1, len(sys.argv)):
        if sys.argv[i] == 'refresh':
            refresh = True
        elif sys.argv[i].startswith('http'):
            refresh_from_cl.append(sys.argv[i])       
    
    # Whether to prune the database
    prune_database = cfg.getboolean('FILES', 'Prune Database', fallback = False)
    
    # get site list
    sitelist_file = cfg.get('FILES', 'Sitelist File', fallback = 'sitelist.txt')
    site_list_path = os.path.normpath(script_directory + '/' + sitelist_file)
    sitelist = []
    if os.path.exists(site_list_path):
        with open(site_list_path, 'r') as site_list_file:
            sitelist = site_list_file.read().splitlines()
    else:
        print(site_list_path + " missing! Exiting.")
        exit()
    
    # get artists from artists_to_highlight.txt (not essential)
    highlights_path = os.path.normpath(script_directory + '/artists_to_highlight.txt')
    artists_to_highlight=[]
    if os.path.exists(highlights_path):
        with open(highlights_path, 'r') as highlights:
            artists_to_highlight = highlights.read().splitlines()
    
    # Instantiate a page
    aggregator_page = AggregatorPage(cfg)
    # Set list of artists to highlight (these artists will appear in
    # the artists block on the page if this option is selected)
    aggregator_page.setArtistsToHighlight(artists_to_highlight)
    
    # Create / connect to releases database & create table as necessary
    database_path = os.path.normpath(script_directory + '/' + 'releases.db')
    connection = sqlite3.connect(database_path)
    connection.row_factory = sqlite3.Row 
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS releases (guid TEXT PRIMARY KEY, date_simple TEXT, date_full TEXT, artist TEXT, title TEXT, link TEXT, site TEXT)")
    
    # Request and parse feeds if refreshing
    if refresh:
        # If website addresses were provided from the command line
        if len(refresh_from_cl) > 0:
            # Don't prune database if from websites provided via command line (as this could delete most of the database)
            prune_database = False
            for website in refresh_from_cl:
                could_parse = refreshFeed(cfg, website, cursor)
                # if refreshing sites provided on the command line, add site to sitelist.txt if it's not there    
                if could_parse:
                    addSiteToSitelistFile(sitelist, site_list_path, website)
        else:
            for website in sitelist:
                refreshFeed(cfg, website, cursor)
    
    # prune database. Delete any entries which have guid not related to our current site list
    if prune_database:
        pruneDatabase(sitelist, cursor)
    
    # commit changes to database
    connection.commit()
    
    # Next, we order the database by date (descending) and get the rows for our page
    sql = '''SELECT date_full, artist, title, link, site FROM releases ORDER BY date_simple DESC LIMIT ?'''
    maximum_releases_to_display = cfg.getint('RELEASES', 'Maximum Releases To Display', fallback = 100)
    latest_releases = cursor.execute(sql, (maximum_releases_to_display,))
    result = latest_releases.fetchall()
    
    # check we actually returned rows
    if len(result) != 0:
        # Generate release html from each row
        for row in result:
            aggregator_page.generateRelease(row)
        
        # The page is ready to be generated
        aggregator_page.generatePage()
        
        # Save the page to disk
        save_as = cfg.get('FILES', 'Save As', fallback = 'aggregator.html')
        index_destination_path = os.path.normpath(script_directory + '/' + save_as)
        aggregator_page.save(index_destination_path)
        
    else:
        print("Releases database contains no entries. Page not generated.")
        
    cursor.close()
    connection.close()

def pruneDatabase(sitelist, cursor):
    all_guids = set()
    session_guids = set()
    
    # get all guids in database
    sql = '''SELECT guid FROM releases'''
    db_guids = cursor.execute(sql)
    while True:
        result = db_guids.fetchmany(100)
        if not result:
            break
        for row in result:
            all_guids.add(row['guid'])
    
    # get guids from database which are associated with
    # the sites in our site list (session_guids)
    for line in sitelist:
        if line.strip() != "":
            site = stripProtocolAndTrailingSlash(line)
            site = '%' + site + '%'
            sql = '''SELECT guid FROM releases WHERE site LIKE ?'''
            db_guids = cursor.execute(sql, (site,))
            while True:
                result = db_guids.fetchmany(100)
                if not result:
                    break
                for row in result:
                    session_guids.add(row['guid'])
    
    # delete from database if guid not in session_guids
    guids_to_delete = all_guids.difference(session_guids)
    for g in guids_to_delete:
        sql = '''DELETE FROM releases WHERE guid=?'''
        cursor.execute(sql, (g,))

def addSiteToSitelistFile(sitelist, site_list_path, website):
    one_site = stripProtocolAndTrailingSlash(website)
    in_list = False
    for line in sitelist:
        if one_site in line:
            in_list = True
            break
    if not in_list:
        if os.path.exists(site_list_path):
            # check if we need to add a newline
            last_newline = False
            with open(site_list_path, 'rb') as site_list_file_obj:
                site_list_file_obj.seek(0, os.SEEK_END)
                loc = site_list_file_obj.tell()
                if loc > 0:
                    site_list_file_obj.seek(loc-1)
                    this_byte = site_list_file_obj.read(1)
                    if this_byte == b'\n':
                        last_newline = True
                else:
                    last_newline = True
            
            with open(site_list_path, 'a') as site_list_file:
                if not last_newline:
                    site_list_file.write("\n")
                site_list_file.write(website + "\n")


def refreshFeed(cfg, website, cursor):
    can_parse = False
    unnamed_artist_text = cfg.get('TEXT', 'Unnamed Artists Text', fallback = 'Mystery Artist: click to discover!')
    report_fetched = cfg.getboolean('SCRIPT', 'Report Fetched', fallback = True)
    if website.strip() != "":
        rss_feed = Feed(website)
        # we should have valid rss if can_parse is True... hopefully
        can_parse = rss_feed.fetch(report_fetched)
        if can_parse:
            # parse method needs unnamed_artist_text in case it finds no named artist for a release
            release_data_list = rss_feed.parse(unnamed_artist_text)           
            # release_data format: (guid, simple_date, build_date, artist, title, link_url, website)
            for release_data in release_data_list:
                # Add database entry if unique id (guid) doesn't already exist
                sql = '''INSERT OR IGNORE INTO releases(guid, date_simple, date_full, artist, title, link, site) VALUES(?,?,?,?,?,?,?)'''
                cursor.execute(sql, release_data)
    return can_parse


def stripProtocolAndTrailingSlash(txt):
    if txt[-1] == '/':
        txt = txt[:-1]
    if txt.startswith("https://"):
        txt = txt[8:]
    elif txt.startswith("http://"):
        txt = txt[7:]
    return txt

main()
